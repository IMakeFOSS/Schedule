# [IMakeFOSS](https://fosstodon.org/@imakefoss) schedule and nominations

## Schedule

|Week|Name|IMF  |IUF  |Project/Profession|Website|
|----|----|:---:|:---:|-------|-------|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|

## Nominations

To nominate someone as a curator for [IMakeFOSS](https://fosstodon.org/@imakefoss) (**IMF**) please create a new issue.
If you nominate a FOSS contributor, please add the Mastodon username and the FOSS project.

You can also nominate someone for IUseFOSS (**IUF**). In this case please add Mastodon username and profession (e.g. author, artist, musician).

## Links

1. [IMakeFOSS on Mastodon](https://fosstodon.org/@imakefoss)
2. [IMakeFOSS Website](https://imakefoss.org)